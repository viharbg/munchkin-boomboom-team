def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print("Hi, {0}".format(name))  # Press ⌘F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
# if __name__ == '__main__':
#     print_hi('PyCharm')

print("Euler project")
Eulnum = [i for i in range(1000) if i%3==0 or i%5==0]
print("Euler numbers: ", Eulnum)
sumEulnum = sum(Eulnum)
print("the sum is: ", sumEulnum)
